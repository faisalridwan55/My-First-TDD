from django.test import TestCase
from .views import index, friend_list, add_friend, delete_friend, validate_npm, data_friend_list, details_friend, paginate
from django.test import Client
from django.urls import resolve
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import json
# Create your tests here.

class Lab7UnitTest(TestCase):

    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_addFriend_url_is_exist(self):
        response = Client().get('/lab-7/add-friend')
        self.assertEqual(response.status_code, 301)

    def test_validateNpm_url_is_exist(self):
        response = Client().get('/lab-7/validate-npm')
        self.assertEqual(response.status_code, 301)

    def test_getFriendList_url_is_exist(self):
        response = Client().get('/lab-7/get-friend-list')
        self.assertEqual(response.status_code, 301)

    def test_dataFriendList_url_is_exist(self):
        response = Client().get('/lab-7/data-friend-list')
        self.assertEqual(response.status_code, 301)

    def test_lab7_using_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)

    def test_getFriendList_using_friendList_func(self):
        found = resolve('/lab-7/get-friend-list/')
        self.assertEqual(found.func, friend_list)

    def test_dataFriendList_using_dataFriendList_func(self):
        found = resolve('/lab-7/data-friend-list/')
        self.assertEqual(found.func, data_friend_list)

    def test_validateNPM_using_validate_npm_func(self):
        found = resolve('/lab-7/validate-npm/')
        self.assertEqual(found.func, validate_npm)

    def test_friendList(self):
        response_post = friend_list(self)
        self.assertEqual(response_post.status_code, 200)

    def test_dataFriendList(self):
        Client().post(
			'/lab-7/add-friend/',
			{'name':"faisal", 'npm':"1606"}
		)
        response = Client().get('/lab-7/data-friend-list/')
        self.assertIn("faisal", str(response.content))

    def test_validateNPM(self):
        response = Client().post('/lab-7/validate-npm/', {'npm':"1606838666"})
        a = response.getvalue()
        boolean = json.loads(a)['is_taken']
        self.assertEqual(False, boolean)

    def test_delete_friend(self):
        friend = Friend.objects.create(friend_name="faisal ridwan", npm="1606838666")
        response = Client().post('/lab-7/get-friend-list/delete1/' + str(friend.id) + '/')
        self.assertEqual(response.status_code, 302)
        self.assertNotIn(friend, Friend.objects.all())

    def test_addFriendWithoutAlamatAndOthers(self):
        response_post = Client().post(
			'/lab-7/add-friend/',
			{'name':"faisal", 'npm':"1606"}
		)
        self.assertEqual(response_post.status_code, 200)

    def test_addFriendCompletly(self):
        response_post = Client().post(
			'/lab-7/add-friend/',
			{'name':"faisal", 'npm':"1606", 'alamat':"jalan sinar", 'ttl':"tangerang, 5 mei 1998", 'prodi':"Sistem Informasi"}
		)
        self.assertEqual(response_post.status_code, 200)

    def test_addFriendTwice(self):
        friend = Friend.objects.create(friend_name="faisal", npm="1606")
        Client().post(
			'/lab-7/add-friend/',
			{'name':"faisal", 'npm':"1606"}
		)
        self.assertEqual(1, Friend.objects.all().count())

    def test_invalid_sso(self):
        csui_helper = CSUIhelper()
        csui_helper.instance.username = "fsl"
        csui_helper.instance.password = "lala"
        with self.assertRaises(Exception) as context:
            csui_helper.instance.get_access_token()
        self.assertIn("fsl", str(context.exception))

    def test_get_auth_param_dict(self):
        csui_helper = CSUIhelper()
        auth_param = csui_helper.instance.get_auth_param_dict()
        self.assertEqual(auth_param["client_id"], csui_helper.instance.get_auth_param_dict()["client_id"])

    def test_details_friend(self):
        friend = Friend.objects.create(friend_name="faisal", npm="1606838666", alamat="jalan sinar", ttl="Tangerang, 5 Mei 1998", prodi="Sistem Informasi", pk=1)
        response = details_friend(self, 1)
        self.assertIn("faisal", str(response.content))
        self.assertEqual(response.status_code, 200)

    def test_empty_page(self):
        response = paginate(2, Friend.objects.all())
        self.assertEqual(0, response.end_index())
