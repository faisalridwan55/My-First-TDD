from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json
# Create your views here.

response = {}
csui_helper = CSUIhelper()

def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada

    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    friend_list = Friend.objects.all()
    page = request.GET.get('page')
    mahasiswa = paginate(page, mahasiswa_list)
    response = {"mahasiswa_list": mahasiswa, "friend_list": friend_list}
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def paginate(page, listData):
    paginator = Paginator(listData, 10)
    try:
        mahasiswa = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        mahasiswa = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        mahasiswa = paginator.page(paginator.num_pages)
    return mahasiswa

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

def data_friend_list(request):
    if request.method == "GET":
        friend_list = Friend.objects.all()
        data = serializers.serialize("json", friend_list)
        return HttpResponse(data)

@csrf_exempt
def add_friend(request):
    boolean = len(Friend.objects.filter(npm=request.POST['npm'])) > 0
    if boolean:
        return JsonResponse({"boolean": True});
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        try:
            alamat = request.POST['alamat']
            ttl = request.POST['ttl']
            prodi = request.POST['prodi']
            friend = Friend(friend_name=name, npm=npm, alamat=alamat, ttl=ttl, prodi=prodi)
        except Exception as e:
            friend = Friend(friend_name=name, npm=npm)
        friend.save()
        data = model_to_dict(friend)
        return HttpResponse(data)

def delete_friend(request, friend_id):
    Friend.objects.filter(id=friend_id).delete()
    return HttpResponseRedirect('/lab-7/get-friend-list')

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    boolean = len(Friend.objects.filter(npm=npm)) > 0
    data = {
        'is_taken': boolean  #lakukan pengecekan apakah Friend dgn npm tsb sudah ada
    }
    return JsonResponse(data)

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data

def details_friend(request, friend_id):
    friend = Friend.objects.get(pk=friend_id)
    response['nama'] = friend.friend_name
    response['npm'] = friend.npm
    response['alamat'] = friend.alamat
    response['ttl'] = friend.ttl
    response['prodi'] = friend.prodi
    html ='lab_7/details.html'
    return render(request, html, response)
