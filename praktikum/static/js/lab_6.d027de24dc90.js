//select2
$(document).ready(function() {
    $(".js-example-basic-single").select2();
});

//Chat-box
$(document).ready(function(){
    $("#hideButton").click(function(){
        $(".chat-body").toggle(500);
    });
});

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = 0;
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else if (erase === true){
      print.value = x;
      erase = false;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END
