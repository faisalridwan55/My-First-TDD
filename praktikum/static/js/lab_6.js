storage = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
]

var themes;
if (localStorage.getItem("themes") === null) {
  localStorage.setItem("themes", JSON.stringify(storage));
}
themes = JSON.parse(localStorage.getItem("themes"))
var backgroundColor;
var textColor;
var index;
//fungsi ganti tema
function changeTheme(bColor, tColor){
  $("body").css({"backgroundColor": bColor});
  $("h1").css({"color": tColor});
}
//Index merepresentasikan default themes
//Jika dibuka pertama kali atau pada incognito akan memakai background Indigo
//Jika tidak pertama kali, akan menggunakan background terakhir ketika ditutup 
if (localStorage.getItem("index") === null){
  index = 3;
}else{
  index = JSON.parse(localStorage.getItem("index"));
}
//Implementasi pergantian tema
backgroundColor = themes[index]["bcgColor"];
textColor = themes[index]["fontColor"];
// console.log(storage[index]["bcgColor"]);
changeTheme(backgroundColor, textColor);

$(document).ready(function(){
  $(".my-select").select2({
    "data" : themes
  });
  $(".my-select").val(index).change();
  $(".apply-button").on("click", function(){  // sesuaikan class button
      // [TODO] ambil value dari elemen select .my-select
      index = $(".my-select").val();
      // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
      backgroundColor = themes[index]["bcgColor"];
      textColor = themes[index]["fontColor"];
      // [TODO] ambil object theme yang dipilih
      // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
      changeTheme(backgroundColor, textColor);
      // [TODO] simpan object theme tadi ke local storage selectedTheme
      localStorage.setItem("index", JSON.stringify(index));
  })
});

//Chat-box toggle
$(document).ready(function(){
    $("#hideButton").click(function(){
        $(".chat-body").toggle(500);
    });
});

//Chat-box enter message
$(".chat-text").keypress(function(e) {
    if(e.which == 13) {
      var message = $("textarea#message").val();
      var tambah = "<div class='msg-send'>"+message+"</div>";
      $("div.space").append(tambah);
      $(".chat-body").animate({scrollTop: $(".chat-body")[0].scrollHeight}, 'fast');
      $("textarea#message").val("");
      chatBot(message);
    }
});

//Buat chat bot
function chatBot(message) {
	const corsProxy = 'https://cors-anywhere.herokuapp.com/';
	const simiURL = 'http://sandbox.api.simsimi.com/request.p?key=deba5d85-2a4e-449c-847b-90d5667542fd&lc=id&ft=1.0&text='+message;
	$.get(corsProxy + simiURL, function(data) {
		$("div.space").append("<div class='msg-receive'>"+data.response+"</div>");
		//auto scroll to the latest chat
		$(".chat-body").animate({scrollTop: $(".chat-body")[0].scrollHeight}, 'fast');
	});
};

// Calculator
var print = document.getElementById("print");
var erase = false;

var go = function(x) {
  if (x === "ac") {
    /* implemetnasi clear all */
    print.value = 0;
    erase = true;
  } else if (x === "eval") {
    try {
        print.value = Math.round(evil(print.value) * 10000) / 10000;
        erase = true;
    } catch (SyntaxError) {
        print.value = "Input Salah";
    }
  } else if (erase === true){
      print.value = x;
      erase = false;
  } else if (x === 'log') {
  		print.value = Math.log10(print.value);
      erase = true;
  } else if (x === 'sin') {
  		print.value = Math.sin(print.value * Math.PI / 180);
      erase = true;
  		if(print.value == 1.2246467991473532e-16) print.value = 0;
  } else if (x === 'tan') {
  		print.value = Math.tan(print.value * Math.PI / 180);
      erase = true;
  		if(print.value == -1.2246467991473532e-16){
        print.value = 0;
  		}else if(print.value == 0.9999999999999999){
        print.value = 1;
      }
  }else {
      print.value += x;
  }
};

function evil(fn) {
  return new Function("return " + fn)();
}
// END
